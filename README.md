# ci-templates

This project stores CI templates which can be included in all pipelines in order
to ensure consistency, and to make it easier to setup new projects.

## Usage

In your projects `.gitlab-ci.yml` file add this section:

```yaml
include:
  - project: "perobertson/ci-templates"
    ref: main
    file: "/templates/default.gitlab-ci.yml"
```

## Workflow

### Default

```mermaid
flowchart LR
  build:cargo_doc:release-->pages

  build:cargo_build:release:arch-->build:img:tag
  build:shellcheck-->build:img:tag

  build:cargo_build:release:arch-->build:img:arch
  build:img:tag-->build:img:arch
  build:img:arch-->build:img:release
  build:img:release-->test:img:release
  deploy:cargo_package:release:arch-->deploy:img:release
  test:img:release-->deploy:img:release

  build:cargo_audit-->build:cargo_build:release:arch
  build:cargo_build:release:arch-->build:cargo_doc:release
  build:cargo_build:release:arch-->deploy:cargo_package:release:arch
  build:cargo_build:release:arch-->test:cargo_clippy:release
  build:cargo_build:release:arch-->test:cargo_test:release
  build:cargo_fmt-->build:cargo_build:release:arch
  build:cargo_machete-->build:cargo_build:release:arch
  test:cargo_clippy:release-->deploy:cargo_package:release:arch
  test:cargo_test:release-->deploy:cargo_package:release:arch

  deploy:img:release-->deploy:helm:package:$CHART_NAME
  deploy:helm:package:$CHART_NAME-->deploy:helm:publish-chart:$CHART_NAME
  deploy:cargo_package:release:arch-->deploy:helm:publish-chart:$CHART_NAME
  deploy:helm:test-docs:$CHART_NAME-->deploy:helm:publish-chart:$CHART_NAME
  deploy:helm:package:$CHART_NAME-->deploy:helm:lint:$CHART_NAME
  deploy:helm:lint:$CHART_NAME-->deploy:helm:publish-chart:$CHART_NAME
  test:yamllint-->deploy:helm:publish-chart:$CHART_NAME

  build:cargo_build:release:arch-->deploy:release
  deploy:cargo_package:release:arch-->deploy:release
  deploy:img:release-->deploy:release

  build:shellcheck
  test:tofu:format
  test:tofu:provider_lock
  test:tofu:validate
```

## Known work

- [ ] `deploy:release` should depend on all the release types
